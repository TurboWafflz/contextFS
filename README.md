# ContextFS

A very strange storage format I created that stores multiple text files in a single file and compresses them by replacing any lines that appear multiple times with a reference to the first occurrence. It can also store binary files, however, they will not be compressed (Unless there are multiple identical files in which case the second copy will become a reference to the first)

## Usage
Import the module in Python 3 by using `import cxfs` (Make sure the cxfs.py file is in Python's path)

Create a filesystem object `myFS = cxfs.fs()`

Set the location of the filesystem `myFS.location = "example.cxfs"`

Write a text file `myFS.write("helloImAFile", "hello CXFS!")`

Read a text file `myFS.read("helloImAFile")`

Delete a file `myFS.delete("helloImAFile")`

List files `myFS.listFiles()`

Write a binary file `myFS.writeBin("helloImABinFile", b'wow you could put the bytes for an image or something here but I just put some UTF-8 text')`

Read a binary file `myFS.readBin("helloImABinFile")`

Write all files in a directory `myFS.writeDir("/home/fmw/thisIsADirectoryOnMyComputer")`

Read all files to a directory `myFS.readToDir("/home/fmw/thisIsADirectoryOnMyComputer")`